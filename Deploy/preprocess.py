import pickle
import sys
sys.path.append('')

from Transformers.transformers import *
from sklearn.pipeline import make_pipeline


# implementing transformation
def main():
    get_number_of_week = ['day']
    remove_negatives = ['pdays']
    dummy_features = ['job', 'marital', 'education', 'contact', 'month', 'poutcome']
    yes_no_features = ['default', 'housing', 'loan']
    log_features = ['duration', 'previous', 'campaign']
    standarize = ['age', 'balance', 'duration', 'pdays', 'previous', 'campaign']

    # path_to_file = '../data/bank-full.csv'
    path_to_file = 'data/bank-full.csv'
    df = pd.read_csv(path_to_file, sep=';')
    print(df.head(5))
    # df = YesNoDecoder().fit_transform(df)

    union = make_union(
        make_pipeline(
            FeatureSelector(get_number_of_week),
            WeekTransformer(),
            GetDummies()
        ),
        make_pipeline(
            FeatureSelector(remove_negatives),
            RemoveNegatives(),
            Log1pTransformer()
        ),
        make_pipeline(
            FeatureSelector(dummy_features),
            GetDummies()
        ),
        make_pipeline(
            FeatureSelector(yes_no_features),
            YesNoDecoder()
        ),
        make_pipeline(
            FeatureSelector(log_features),
            Log1pTransformer()
        ),
        make_pipeline(
            FeatureSelector(standarize),
            DFStandardScaler()
        )
    )
    union.fit(df)

    with open('pipeline.pkl', 'wb') as f:
        pickle.dump(union, f)


if __name__ == '__main__':
    main()
