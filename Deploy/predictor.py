import os
import pickle
import pandas as pd
from utils import IPredictable, load


class MyPredictor(IPredictable):
    """An example Predictor for an AI Platform custom prediction routine."""

    def __init__(self, model, preprocessor):
        """Stores artifacts for prediction. Only initialized via `from_path`.
        """
        self._model = model
        self._preprocessor = preprocessor

    def predict(self, instances, **kwargs):
        """Performs custom prediction.

        Preprocesses inputs, then performs prediction using the trained Keras
        model.

        Args:
            instances: A list of prediction input instances.
            **kwargs: A dictionary of keyword args provided as additional
                fields on the predict request body.

        Returns:
            A list of outputs containing the prediction results.
        """
        # convert .json to dataframe
        data = pd.DataFrame(instances)

        preprocessed_inputs = self._preprocessor.transform(data)

        predictions = self._model.predict(preprocessed_inputs)

        predictions_df = pd.DataFrame(
            predictions,
            index=data.index,
            columns=['Predicted_Class']
        )
        # return outputs.tolist()
        return predictions_df.to_json(orient='records')

    @classmethod
    def from_path(cls, model_dir):
        """Creates an instance of MyPredictor using the given path.

        This loads artifacts that have been copied from your model directory in
        Cloud Storage. MyPredictor uses them during prediction.

        Args:
            model_dir: The local directory that contains the trained Keras
                model and the pickled preprocessor instance. These are copied
                from the Cloud Storage model directory you provide when you
                deploy a version resource.

        Returns:
            An instance of `MyPredictor`.
        """
        model_path = os.path.join(model_dir, 'model.pkl')
        model = load(model_path)
        preprocessor_path = os.path.join(model_dir, 'pipeline.pkl')
        preprocessor = load(preprocessor_path)

        return cls(model, preprocessor)