import sys
sys.path.append('')

from sklearn.neighbors import KNeighborsClassifier as KNN
from Transformers.transformers import YesNoDecoder
from sklearn.model_selection import train_test_split

import pickle
import pandas as pd
import logging

LOG_LEVEL = 'INFO'
logging.basicConfig(level=logging.getLevelName(LOG_LEVEL), format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def train_model(in_xtrain: pd.DataFrame, in_ytrain):
    logging.info("train_model()")
    knn = KNN(n_neighbors=3)
    knn.fit(in_xtrain, in_ytrain)
    return knn


def save_pickle(in_model, in_model_name: str):
    logging.debug("save_pickle")
    with open(in_model_name, 'wb') as f:
        pickle.dump(in_model, f)


# function just to print score of a model
def test_prediction(in_model, in_xtest, in_ytest):
    logging.debug("test_prediction")
    # making predictions
    score = in_model.score(in_xtest, in_ytest)
    print("Test score: {0:.2f} %".format(100 * score))
    Ypredict = in_model.predict(in_xtest)
    print(Ypredict)


def main():
    logging.debug("main")
    # load and split
    data_path = 'data/bank-full.csv'
    df = pd.read_csv(data_path, sep=';')
    with open('pipeline.pkl', 'rb') as file:
        pickle_pipeline = pickle.load(file)

    print(pickle_pipeline)
    print(type(pickle_pipeline))

    X_train, X_test, y_train, y_test = train_test_split(df.drop(['y'], axis=1), df['y'])

    # change labels to 0/1 instead of yes/no
    y_train_T = YesNoDecoder().fit_transform(y_train)
    y_test_T = YesNoDecoder().fit_transform(y_test)

    pickle_pipeline.fit(X_train)

    X_train_T = pickle_pipeline.transform(X_train)
    X_test_T = pickle_pipeline.transform(X_test)
    print(X_train_T.head(5))

    # training model
    model = train_model(X_train_T, y_train_T)

    # printing scores
    test_prediction(in_model=model, in_xtest=X_test_T, in_ytest=y_test_T)

    # save model to pickle file
    save_pickle(in_model=model, in_model_name='model.pkl')


if __name__ == '__main__':
    main()