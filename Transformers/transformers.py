import pandas as pd
import numpy as np

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import OneHotEncoder, FunctionTransformer, StandardScaler
from sklearn.externals.joblib import Parallel, delayed
from sklearn.pipeline import FeatureUnion, _fit_transform_one, _transform_one, _name_estimators
from scipy import sparse

# dffunction transformer
class DFFunctionTransformer(TransformerMixin):
    # FunctionTransformer but for pandas DataFrames

    def __init__(self, *args, **kwargs):
        self.ft = FunctionTransformer(*args, **kwargs)

    def fit(self, X, y=None):
        # stateless transformer
        return self

    def transform(self, X):
        Xt = self.ft.transform(X)
        Xt = pd.DataFrame(Xt, index=X.index, columns=X.columns)
        return Xt


"""Transformer extracts columns passed as argument - selects given features."""
class FeatureSelector(BaseEstimator, TransformerMixin):
    # class constructor
    def __init__(self, feature_names):
        self._feature_names = feature_names

    def fit(self, X, y=None):
        return self

    # what we need this transformer to do
    def transform(self, X, y=None):
        return X[self._feature_names]


class GetDummies(BaseEstimator, TransformerMixin):
    def __init__(self, drop=None, sparse=True, dtype=int, handle_unknown='ignore'):
        self._drop = drop
        self._sparse = sparse
        self._dtype = dtype
        self._handle_unknown = handle_unknown

    def fit(self, X: pd.DataFrame, y=None):
        self._Encoder = OneHotEncoder(sparse=self._sparse, dtype=self._dtype, handle_unknown=self._handle_unknown)
        self._Encoder.fit(X)
        return self

    def transform(self, X: pd.DataFrame, y=None):
        index = X.index
        transformed = self._Encoder.transform(X)
        columns = [item for sublist in self._Encoder.categories_ for item in sublist]
        return pd.DataFrame(transformed.toarray(), columns=columns, index=index)
    # def __init__(self):
    #     pass
    #
    # def fit(self, X: pd.DataFrame, y=None):
    #     return self
    #
    # def transform(self, X: pd.DataFrame, y=None):
    #     return pd.get_dummies(data=X)



# just for column pdays


class RemoveNegatives(TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X = X.replace(-1, 0)
        return X


class YesNoDecoder(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X.replace(('yes', 'no'), (1, 0), inplace=True)
        return X


class Log1pTransformer(TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        Xlog = np.log1p(X)
        return Xlog


class DFStandardScaler(TransformerMixin):
    def __init__(self):
        self.ss = None

    def fit(self, X, y=None):
        self.ss = StandardScaler().fit(X)
        return self

    def transform(self, X):
        Xss = self.ss.transform(X)
        XScaled = pd.DataFrame(Xss, index=X.index, columns=X.columns)
        return XScaled


"""Class that get number of a week from days."""
class WeekTransformer(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    # what we need this transformer to do
    def transform(self, X, y=None):
        week_first = []
        week_second = []
        week_third = []
        week_fourth = []

        for day in X['day']:
            if day <= 7:
                week_first.append(1)
                week_second.append(0)
                week_third.append(0)
                week_fourth.append(0)

            elif 8 <= day < 15:
                week_first.append(0)
                week_second.append(1)
                week_third.append(0)
                week_fourth.append(0)
            elif 16 <= day < 23:
                week_first.append(0)
                week_second.append(0)
                week_third.append(1)
                week_fourth.append(0)
            else:
                week_first.append(0)
                week_second.append(0)
                week_third.append(0)
                week_fourth.append(1)

        X = X.copy()

        X['week_first'] = week_first
        X['week_second'] = week_second
        X['week_third'] = week_third
        X['week_fourth'] = week_fourth

        X = X.drop('day', axis=1)
        Xweek = X
        return Xweek


"""Pandas feature union"""
class PandasFeatureUnion(FeatureUnion):
    def fit_transform(self, X, y=None, **fit_params):
        self._validate_transformers()
        result = Parallel(n_jobs=self.n_jobs)(
            delayed(_fit_transform_one)(
                transformer=trans,
                X=X,
                y=y,
                weight=weight,
                **fit_params)
            for name, trans, weight in self._iter())

        if not result:
            # All transformers are None
            return np.zeros((X.shape[0], 0))
        Xs, transformers = zip(*result)
        self._update_transformer_list(transformers)
        if any(sparse.issparse(f) for f in Xs):
            Xs = sparse.hstack(Xs).tocsr()
        else:
            Xs = self.merge_dataframes_by_column(Xs)
        return Xs

    def merge_dataframes_by_column(self, Xs):
        return pd.concat(Xs, axis="columns", copy=False)

    def transform(self, X):
        Xs = Parallel(n_jobs=self.n_jobs)(
            delayed(_transform_one)(
                transformer=trans,
                X=X,
                y=None,
                weight=weight)
            for name, trans, weight in self._iter())
        if not Xs:
            # All transformers are None
            return np.zeros((X.shape[0], 0))
        if any(sparse.issparse(f) for f in Xs):
            Xs = sparse.hstack(Xs).tocsr()
        else:
            Xs = self.merge_dataframes_by_column(Xs)
        return Xs


def make_union(*transformers, **kwargs):
    n_jobs = kwargs.pop('n_jobs', None)
    verbose = kwargs.pop('verbose', False)
    if kwargs:
        # We do not currently support `transformer_weights` as we may want to
        # change its type spec in make_union
        raise TypeError('Unknown keyword arguments: "{}"'
                        .format(list(kwargs.keys())[0]))
    return PandasFeatureUnion(
        _name_estimators(transformers), n_jobs=n_jobs)