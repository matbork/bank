#todo import libraries
import pickle
from setuptools import setup, find_packages
import os
import logging
import re

LOG_LEVEL = 'INFO'
logging.basicConfig(level=logging.getLevelName(LOG_LEVEL), format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def _list_dir(directory: str):
    logging.info("package_files()")
    paths = []
    pattern_to_exclude = re.compile(r"__pycache__"
                                    r"|\.DS_Store")

    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            path_to_append = os.path.join('..', path, filename)
            if not pattern_to_exclude.search(path_to_append):
                paths.append(path_to_append)

    logging.info(paths)
    return paths


def package_directories(dirs: list) -> dict:
    """
    method to pack file paths into directories dict
    Args:
        dirs: list of directories to add to source distribution

    Returns:
        dict of directories names with files included in them
    """
    logging.debug("package_directories()")
    return {x: _list_dir(x) for x in dirs}


def deploy():
    logging.debug("deploy()")
    with open('ver.file', mode='r', encoding='utf-8', buffering=1) as file:
        lines = file.readlines()
        model_name = lines[0].replace('\n', '')
        version = lines[1].replace('\n', '')

    list_of_directories = [
        'Deploy',
        'Transformers'
    ]

    setup(
        version=version,
        name=model_name,
        scripts=["utils.py", 'ver.file'],
        packages=list_of_directories,
        package_data=package_directories(list_of_directories)
    )
    return


if __name__ == '__main__':
    deploy()

