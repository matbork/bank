import logging
from os import getenv
from itertools import cycle
import pickle

ENCODING = getenv('ENCODING', 'utf-8')
LOG_LEVEL = getenv('LOG_LEVEL', 'INFO')
logging.basicConfig(level=logging.getLevelName(LOG_LEVEL),
                    format='%(asctime)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


def _xor(data: str, key: str) -> str:
    logging.debug("_xor")
    return ''.join(chr(ord(x) ^ ord(y)) for (x, y) in zip(data, cycle(key)))


def decode(cipher: str, key: str) -> str:
    logging.debug("decode")
    return _xor(cipher, key)


def encode(data: str, key: str) -> str:
    logging.debug("encode")
    return _xor(data, key)


if __name__ == '__main__':
    key = '2137'
    text = 'A można, jak najbardziej, jeszcze jak!'
    result = _xor(text, key)

    logging.info(result)
    logging.info(type(result))
    logging.info("result")

    result2 = _xor(result, key)
    logging.info(result2)
    logging.info(type(result2))
    logging.info("result2")


def load(file_path: str) -> object:
    logging.debug("load")
    with open(file_path, mode='rb') as file:
        obj = pickle.load(file)
        logging.debug(obj)
        logging.debug(type(obj))
        logging.debug("obj")

    return obj


def save(file_path: str, obj: object):
    logging.debug("save")
    with open(file_path, mode='wb') as file:
        pickle.dump(obj, file)
    return


class IPredictable(object):
    """Interface for constructing custom predictors."""

    def predict(self, instances, **kwargs):
        """Performs custom prediction.

        Instances are the decoded values from the request. They have already
        been deserialized from JSON.

        Args:
            instances: A list of prediction input instances.
            **kwargs: A dictionary of keyword args provided as additional
                fields on the predict request body.

        Returns:
            A list of outputs containing the prediction results. This list must
            be JSON serializable.
        """
        raise NotImplementedError()

    @classmethod
    def from_path(cls, model_dir):
        """Creates an instance of Predictor using the given path.

        Loading of the predictor should be done in this method.

        Args:
            model_dir: The local directory that contains the exported model
                file along with any additional files uploaded when creating the
                version resource.

        Returns:
            An instance implementing this Predictor class.
        """
        raise NotImplementedError()
