# TODO $1: model name visible on AI platform
# TODO $2: version of deployed model
#  --runtime-version: version of deploy environment, reference: https://cloud.google.com/ai-platform/prediction/docs/runtime-version-list
# --python-version: refer to link above for python versions available
# todo --origin $3: bucket path
# --prediction-class name of custom Predictor class, format: module_name.ClassName

# before running install gcloud beta `gcloud components install beta`;

python3 Deploy/preprocess.py
python3 Modeling/model.py


#BUCKET_PATH=$3$1/$2
# $1
MODEL_NAME="knn_test_2"
# $2
VERSION="v0_5"
# $3
BUCKET_NAME="gs://mateusz-test"

echo $MODEL_NAME >ver.file  # model name
echo $VERSION >>ver.file # model version

cd ./dist/ && ls | xargs rm && cd ../
python3 setup.py sdist --formats=gztar
mv ./dist/*.tar.gz ./dist/dependencies.tar.gz

# BUCKET_PATH=$3$1/$2
BUCKET_PATH=$BUCKET_NAME/$MODEL_NAME/$VERSION
gsutil cp model.pkl $BUCKET_PATH/model.pkl
gsutil cp pipeline.pkl $BUCKET_PATH/pipeline.pkl
gsutil cp ./dist/dependencies.tar.gz $BUCKET_PATH/dependencies.tar.gz
gsutil ls $BUCKET_PATH

# storage name: mateusz-test
# model name: test_model

## deploy model
gcloud beta ai-platform versions create $VERSION \
  --model $MODEL_NAME \
  --runtime-version 1.15 \
  --python-version 3.7 \
  --origin $BUCKET_PATH/ \
  --package-uris $BUCKET_PATH/dependencies.tar.gz \
  --prediction-class Deploy.predictor.MyPredictor


# todo check this
# predictions = gcloud ai-platform predict --model=MODEL_NAME --text-instanctes=INPUT_FILE